package com.saurabi.online.user.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import com.saurabi.online.user.serviceImpl.JpaUserDetailsManager;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JpaUserDetailsManager jpaUserDetailsManager;

	@Configuration
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
		protected void configure(HttpSecurity http) throws Exception {
            
			http.csrf().disable().authorizeRequests().antMatchers("/**").permitAll();
			
//            http.csrf().disable().authorizeRequests()
//			.antMatchers("/adminControl/**").hasRole("ADMIN")
//			.antMatchers("/userControl/**").hasRole("USER").anyRequest().authenticated()
//			.and().httpBasic();

		}
	}

//	@Configuration
//	@Order(2)
//	public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
//
//		@Override
//		protected void configure(HttpSecurity http) throws Exception {
//			http.csrf().disable().authorizeRequests()
//			.antMatchers("/logout", "/login").permitAll()
//			.anyRequest().authenticated().and().formLogin().permitAll() 
//			 .and().logout(logout -> logout
//			            .logoutUrl("/logout")
//			            .addLogoutHandler(new SecurityContextLogoutHandler()))
//			;
//		}
//	}

//	private static final String[] AUTH_WHITELIST = { "/swagger-resources/**", "/swagger-ui/**", "/swagger-ui.html",
//			"/v2/api-docs", "/webjars/**","/user/registerUser" };
//
//	@Override
//	@Order(3)
//	public void configure(WebSecurity web) throws Exception {
//		web.ignoring().antMatchers(AUTH_WHITELIST);
//	}

	@Bean
	public DaoAuthenticationProvider jpaDaoAuthenticationProvider() {
		DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
		daoAuthenticationProvider.setUserDetailsService(jpaUserDetailsManager);
		daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
		return daoAuthenticationProvider;
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(jpaDaoAuthenticationProvider());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
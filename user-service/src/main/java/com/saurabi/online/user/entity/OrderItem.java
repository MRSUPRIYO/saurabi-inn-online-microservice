package com.saurabi.online.user.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "Order_item")
public class OrderItem {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="ORDER_ID")
	private Long orderId;

	@Column(name="ITEM_ID")
	private Long itemId;
	
	@Column(name="PRICE")
	private double price;
	
	@Column(name="QUANTITY")
	private int quantity;
}

package com.saurabi.online.user.service;

import java.util.List;

import com.saurabi.online.user.entity.Item;
import com.saurabi.online.user.entity.Order;
import com.saurabi.online.user.exception.UserException;
import com.saurabi.online.user.model.dto.CreateOrderDTO;
import com.saurabi.online.user.model.dto.ItemDTO;
import com.saurabi.online.user.model.dto.OrderDTO;

public interface RestaurantService {

	Item getItemById(long id);

	List<ItemDTO> getAllItems();

	Order createSingleOrder(String email, CreateOrderDTO orderDetails) throws UserException;

	OrderDTO getFinalBillOfUser(String email) throws UserException;

	List<OrderDTO> getAllBillsGeneratedToday() throws UserException;

	double getTotalSalesOfCurrentMonth();

}
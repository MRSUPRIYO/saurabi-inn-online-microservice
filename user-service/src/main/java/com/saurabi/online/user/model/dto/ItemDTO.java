package com.saurabi.online.user.model.dto;

import lombok.Data;

@Data
public class ItemDTO {
	
	private Long itemId;

	private String name;
	
	private double price;
}

package com.saurabi.online.user.serviceImpl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saurabi.online.user.entity.Item;
import com.saurabi.online.user.entity.Order;
import com.saurabi.online.user.entity.OrderItem;
import com.saurabi.online.user.entity.User;
import com.saurabi.online.user.exception.UserException;
import com.saurabi.online.user.model.dto.CreateOrderDTO;
import com.saurabi.online.user.model.dto.CreateOrderItemDTO;
import com.saurabi.online.user.model.dto.ItemDTO;
import com.saurabi.online.user.model.dto.OrderDTO;
import com.saurabi.online.user.repository.ItemRepository;
import com.saurabi.online.user.repository.OrderItemRepository;
import com.saurabi.online.user.repository.OrderRepository;
import com.saurabi.online.user.service.RestaurantService;
import com.saurabi.online.user.service.UserService;
import com.saurabi.online.user.utility.AppUtility;

@Service
public class RestaurantServiceImp implements RestaurantService {

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	OrderItemRepository orderItemRepository;

	@Autowired
	UserService userService;

	@Override
	public Item getItemById(long id) {

		final Optional<Item> optionalItem = itemRepository.findById(id);

		if (optionalItem.isPresent()) {
			return optionalItem.get();
		} else {
			throw new UserException(MessageFormat.format("User with id {0} cannot be found.", id));
		}
	}

	@Override
	public List<ItemDTO> getAllItems() {

		List<Item> items = itemRepository.findAll();
		if (items != null) {

			return items.stream().map(AppUtility::getItemDTOFromItem).collect(Collectors.toList());
		}

		return null;
	}

	@Override
	public Order createSingleOrder(String email, CreateOrderDTO orderDetails) throws UserException {

		//String email = AppUtility.getLoggedInUserName();

		User userDetails = userService.loadUserByEmail(email);

		Double grandTotal = 0.0;

		List<OrderItem> orderItemList = new ArrayList<OrderItem>();

		for (CreateOrderItemDTO createOrderItemDTO : orderDetails.getOrderItemList()) {

			Item itemDetails = getItemById(createOrderItemDTO.getItemId());
			Double totalPrice = itemDetails.getPrice() * createOrderItemDTO.getQuantity();

			grandTotal = grandTotal + totalPrice;

			OrderItem orderItemDetails = new OrderItem();
			orderItemDetails.setItemId(createOrderItemDTO.getItemId());
			orderItemDetails.setPrice(totalPrice);
			orderItemDetails.setQuantity(createOrderItemDTO.getQuantity());
			orderItemList.add(orderItemDetails);
		}

		Order newOrder = Order.builder().grandTotal(grandTotal).userDetails(userDetails).createdAt(new Date()).build();

		Order createdOrder = orderRepository.saveAndFlush(newOrder);

		for (OrderItem orderdItems : orderItemList) {

			orderdItems.setOrderId(createdOrder.getId());
		}
		orderItemRepository.saveAll(orderItemList);
		orderItemRepository.flush();

		return createdOrder;
	}

	@Override
	public OrderDTO getFinalBillOfUser(String email) throws UserException {

		//String email = AppUtility.getLoggedInUserName();

		User userDetails = userService.loadUserByEmail(email);
		
		long finalOrderId = orderRepository.getMaxOrderIdByUserId(userDetails.getUser_id());
		
		if (finalOrderId != 0) {
			final Optional<Order> finalOrder = orderRepository.findById(finalOrderId);

			if (finalOrder.isPresent()) {
				return AppUtility.getOrderDTOFromOrder(finalOrder.get());
			} else {
				throw new UserException(MessageFormat.format("Order with id {0} cannot be found.", finalOrderId));
			}
		}

		else {
			throw new UserException(MessageFormat.format("Order details not found for user {0} .", email));
		}
	}

	@Override
	public List<OrderDTO> getAllBillsGeneratedToday() throws UserException {
		List<Order> allOrdersGeneratedToday = orderRepository.findAllByCreatedAt();
		
		if (allOrdersGeneratedToday != null) {

			return allOrdersGeneratedToday.stream().map(AppUtility::getOrderDTOFromOrder).collect(Collectors.toList());
		}
		
		return null;
	}

	@Override
	public double getTotalSalesOfCurrentMonth() {
		
		double totalSalesOfThisMonth=0.0;
		
		List<Order> allOrdersGeneratedThisMonth = orderRepository.getTotalSalesOfCurrentMonth();
		
		for(Order orderOfThisMonth:  allOrdersGeneratedThisMonth ) {
			
			totalSalesOfThisMonth = totalSalesOfThisMonth+orderOfThisMonth.getGrandTotal();
		}
		
		return totalSalesOfThisMonth;
	}

}

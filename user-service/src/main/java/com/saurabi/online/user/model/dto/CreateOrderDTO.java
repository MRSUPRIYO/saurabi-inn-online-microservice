package com.saurabi.online.user.model.dto;

import java.util.List;

import lombok.Data;

@Data
public class CreateOrderDTO {

	private List<CreateOrderItemDTO> orderItemList;
}

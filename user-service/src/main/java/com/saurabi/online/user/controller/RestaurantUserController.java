/**
 * 
 */
package com.saurabi.online.user.controller;

import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.saurabi.online.user.entity.Order;
import com.saurabi.online.user.model.dto.CreateOrderDTO;
import com.saurabi.online.user.model.dto.ItemDTO;
import com.saurabi.online.user.model.dto.OrderDTO;
import com.saurabi.online.user.entity.User;
import com.saurabi.online.user.entity.UserRole;
import com.saurabi.online.user.exception.UserException;
import com.saurabi.online.user.model.dto.UserDTO;
import com.saurabi.online.user.model.dto.UserDetailsDTO;
import com.saurabi.online.user.utility.AppUtility;

/**
 * @author Supriyo M
 *
 */
@RestController
public class RestaurantUserController {
	
	@Autowired
	com.saurabi.online.user.service.UserService UserService;
	
	@Autowired
	com.saurabi.online.user.service.RestaurantService  restaurantService;

	/**
	 * @param email
	 * @return
	 */
	public boolean checkIfUserExistsByEmail(String email) {
		return UserService.userExists(email);
	}

	/**
	 * @param userDetails
	 * @return
	 */
	@PostMapping("user/registerUser")
	public ResponseEntity<String> registerUser(@Valid @RequestBody UserDetailsDTO userDetails) {

		if (!checkIfUserExistsByEmail(userDetails.getEmail())) {
			
			UserDTO userDTO = AppUtility.getUserDTOFromUserDetailsDTO(userDetails);
			
			userDTO.setAuthorities(Collections.singleton("ROLE_".concat(UserRole.USER.name())));
			
			final User createdUser = UserService.createSingleUser(userDTO);
			if(createdUser != null) {
				return new ResponseEntity<String>("User registered successfully", HttpStatus.CREATED);
			}else {
				return new ResponseEntity<String>("User registration Failed!!", HttpStatus.CONFLICT);
			}
		} else {
			throw new UserException(
					userDetails.getEmail().concat(" already exists. Please try with a different user"));
		}
	}
	
	/**
	 * @return
	 */
	@GetMapping("/")
	public String home() {
		return "<h1>Saurabhi Inn online</h1>";
		
	}
	
	/**
	 * @return
	 */
	@GetMapping("userControl/viewMenu")
	public List<ItemDTO> viewMenu() {
		return restaurantService.getAllItems();		 	
	}
	
	
	/**
	 * @param orderDetails
	 * @return
	 */
	@PostMapping("userControl/placeOrder")
	public ResponseEntity<String> placeOrder(@Valid @RequestBody CreateOrderDTO orderDetails,String email) {
		
		Order newOrder = restaurantService.createSingleOrder(email,orderDetails);
		if(newOrder != null) {
			return new ResponseEntity<String>("Order placed successfully", HttpStatus.CREATED);
		}else {
			return new ResponseEntity<String>("Order creation Failed!!", HttpStatus.CONFLICT);
		}		
	}
	
	/**
	 * @return
	 */
	@GetMapping("userControl/viewFinalBill")
	public OrderDTO viewFinalBill(String email) {
		return restaurantService.getFinalBillOfUser(email);	 	
	}
	
	/**
	 * @param userException
	 * @return
	 */
	@ExceptionHandler(value = UserException.class)
	public ResponseEntity<String> handleUserException(
			UserException userException) {
		return new ResponseEntity<String>(userException.getMessage(), HttpStatus.CONFLICT);
	}
}

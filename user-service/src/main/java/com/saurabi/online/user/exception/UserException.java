package com.saurabi.online.user.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class UserException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1546328193662397854L;
	private String message;

}

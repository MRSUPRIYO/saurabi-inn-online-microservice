package com.saurabi.online.user.repository;

import org.springframework.data.repository.CrudRepository;

import com.saurabi.online.user.entity.Authority;


public interface AuthorityRepository extends CrudRepository<Authority, Long>{

}

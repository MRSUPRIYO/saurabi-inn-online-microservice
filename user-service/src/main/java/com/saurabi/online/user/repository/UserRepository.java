package com.saurabi.online.user.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.saurabi.online.user.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

	Optional<User> findByEmail(String email);
}

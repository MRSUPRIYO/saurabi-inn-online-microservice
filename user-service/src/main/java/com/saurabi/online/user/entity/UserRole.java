/**
 * 
 */
package com.saurabi.online.user.entity;

/**
 * @author Supriyo M
 *
 */
public enum UserRole {
	
	ADMIN, USER

}

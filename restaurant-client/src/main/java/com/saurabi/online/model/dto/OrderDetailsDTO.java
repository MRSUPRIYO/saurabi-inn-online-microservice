package com.saurabi.online.model.dto;

import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @author Supriyo M
 *
 */
@Getter
@EqualsAndHashCode
public class OrderDetailsDTO {
	private List<CreateOrderItemDTO> restaurantItemAddItemDTO;

	public OrderDetailsDTO(List<CreateOrderItemDTO> restaurantItemAddItemDTO) {
		this.restaurantItemAddItemDTO = restaurantItemAddItemDTO;
	}

	public OrderDetailsDTO() {
		super();
	}

}
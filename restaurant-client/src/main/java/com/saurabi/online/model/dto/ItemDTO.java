package com.saurabi.online.model.dto;

import lombok.Data;

@Data
public class ItemDTO {
	
	private Long itemId;

	private String name;
	
	private double price;
}

package com.saurabi.online.model.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

@Data 
public class UserDetailsDTO {

	@ApiModelProperty(position = 0, value = "First name of the user", name = "firstName", dataType = "String", example = "Supriyo")
	@NotBlank(message = "First name can not be empty")
	private String firstname;

	@ApiModelProperty(position = 1, value = "Last name of the user", name = "lastname", dataType = "String", example = "Mukherjee")
	@NotBlank(message = "Last name can not be empty")
	private String lastname;

	@ApiModelProperty(position = 2, value = "Email of the user", name = "email", dataType = "String", example = "supriyo.mukherjee@ymail.com")
	@NotBlank(message = "Email can not be empty")
	@Email(message = "Please provide a valid email id")
	private String email;

	@ApiModelProperty(position = 3, value = "Password of the user", name = "password", dataType = "String", example = "password")
	@NotBlank(message = "Password can not be empty")
	private String password;

}

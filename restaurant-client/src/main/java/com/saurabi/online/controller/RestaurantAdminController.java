package com.saurabi.online.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.saurabi.online.model.dto.OrderDTO;
import com.saurabi.online.model.dto.UserDTO;
import com.saurabi.online.service.RestaurantAdminClient;

/**
 * @author Supriyo M
 *
 */

@RestController
@RequestMapping("/admin-service")
public class RestaurantAdminController {
	
	@Autowired	
	private RestaurantAdminClient restaurantAdminClient;
	
	@GetMapping("/showUser")
	public ResponseEntity<UserDTO> showUserByEmail(@RequestParam("email") String email){
		
		return restaurantAdminClient.showUserByEmail(email);		
	}	
	
	@GetMapping("/findUser/{email}")
	public String checkIfUserExists(@PathVariable("email") String email){
		
		return restaurantAdminClient.checkIfUserExists(email);		
	}	
	
	@PostMapping("/createUser")
	public ResponseEntity<String> createUser(@Valid @RequestBody UserDTO userDetails){
		
		return restaurantAdminClient.createUser(userDetails);		
	}	
	
	@PutMapping("/updateUser")
	public ResponseEntity<String> updateUser(@Valid @RequestBody UserDTO userDetails){
		
		return restaurantAdminClient.updateUser(userDetails);		
	}
	
	@DeleteMapping("/deleteUser/{byEmail}")
	public ResponseEntity<String> deleteUser(@PathVariable("byEmail") String email){
		
		return restaurantAdminClient.deleteUser(email);		
	}	
	
	@GetMapping("/showBillsGeneratedToday")
	public List<OrderDTO> showAllBillsGeneratedToday(){
		
		return restaurantAdminClient.showAllBillsGeneratedToday();		
	}	
	
	@GetMapping("/showTotalSalesOfCurrentMonth")
	public String showTotalSalesOfCurrentMonth() {
		
		return restaurantAdminClient.showTotalSalesOfCurrentMonth();		
	}

}

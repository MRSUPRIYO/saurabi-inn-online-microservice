package com.saurabi.online.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.saurabi.online.model.dto.GeneratedBillsDTO;
import com.saurabi.online.model.dto.OrderDetailsDTO;
import com.saurabi.online.model.dto.OrderDetailsFeedbackDTO;
import com.saurabi.online.service.RestaurantOfflineUserClient;
import com.saurabi.online.utility.AppUtility;

import io.swagger.annotations.ApiOperation;

/**
 * @author Supriyo M
 *
 */
@RestController
@RequestMapping("/offlineuser-service")
public class RestaurantOfflineUserController {
	
	@Autowired	
	private RestaurantOfflineUserClient restaurantOfflineUserClient;

	@PostMapping("/bookseats")
	public String bookSeatsService(@RequestParam("userName") String userName,
			@RequestParam("numberOfSeats") int numberOfSeats,
			@RequestParam("bookDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate bookDate) {
		return restaurantOfflineUserClient.bookSeats(userName, numberOfSeats, bookDate);
	}

	@PostMapping("/feedback")
	public String feedBack(@RequestBody OrderDetailsFeedbackDTO restaurantOrderDetailsFeedbackDTO) {
		return restaurantOfflineUserClient.feedBack(restaurantOrderDetailsFeedbackDTO);
	}

	@PostMapping("/confirmorder")
	public String offlineConfirmOrder(String paymentMode, String orderCityName) {
		return restaurantOfflineUserClient.confirmorder(AppUtility.getLoggedInUserName(),
				paymentMode, orderCityName);
	}
	
	@PostMapping("/addItemstoOrder")
	public String offLineAddItems(@RequestBody OrderDetailsDTO restaurantOrderDetailsDTO) {
		return restaurantOfflineUserClient.addItemstoOrder(AppUtility.getLoggedInUserName(),
				restaurantOrderDetailsDTO);
	}
	
	@GetMapping("/menu")
	public String offLineGetAllItems() {
		return restaurantOfflineUserClient.getAllItems(AppUtility.getLoggedInUserName());
	}

	@GetMapping("/vieworder")
	public List<GeneratedBillsDTO> offLineViewOrder() {
		return restaurantOfflineUserClient.viewOrder(AppUtility.getLoggedInUserName());
	}

}

/**
 * 
 */
package com.saurabi.online.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.saurabi.online.model.dto.CreateOrderDTO;
import com.saurabi.online.model.dto.ItemDTO;
import com.saurabi.online.model.dto.OrderDTO;
import com.saurabi.online.model.dto.UserDetailsDTO;
import com.saurabi.online.service.RestaurantUserClient;
import com.saurabi.online.utility.AppUtility;

/**
 * @author Supriyo M
 *
 */
@RestController
@RequestMapping("/user-service")
public class RestaurantUserController {

	@Autowired	
	private RestaurantUserClient restaurantUserClient;
	
	@GetMapping("/")
	public String home() {
		
		return restaurantUserClient.home();		
	}
	
	@PostMapping("/registerUser")
	public ResponseEntity<String> registerUser(@Valid @RequestBody UserDetailsDTO userDetails){
		
		return restaurantUserClient.registerUser(userDetails);		
	}
	
	@GetMapping("/viewMenu")
	public List<ItemDTO> viewMenu(){
		
		return restaurantUserClient.viewMenu();		
	}
	
	@PostMapping("/placeOrder")
	public ResponseEntity<String> placeOrder(@Valid @RequestBody CreateOrderDTO orderDetails){
		
		//String email = AppUtility.getLoggedInUserName();
		String email ="abishek.gupta@ymail.com";
		return restaurantUserClient.placeOrder(orderDetails,email);		
	}
	
	@GetMapping("/viewFinalBill")
	public OrderDTO viewFinalBill(){
		
		//String email = AppUtility.getLoggedInUserName();
		String email ="abishek.gupta@ymail.com";
		return restaurantUserClient.viewFinalBill(email);		
	}
}

package com.saurabi.online.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.saurabi.online.service.RestaurantFestiveSaleClient;
import com.saurabi.online.utility.AppUtility;


/**
 * @author Supriyo M
 *
 */

@RestController
@RequestMapping("/festivesale-service")
public class RestaurantFestiveSaleController {

	@Autowired	
	private RestaurantFestiveSaleClient restaurantFestiveSaleClient;
	
	@PostMapping("/user/confirmorder")
	public String confirmOrder(String orderCityName,String festiveCode) {
		return restaurantFestiveSaleClient.confirmorder(AppUtility.getLoggedInUserName(), festiveCode, orderCityName); 
				
	}
}

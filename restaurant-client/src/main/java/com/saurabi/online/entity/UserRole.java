/**
 * 
 */
package com.saurabi.online.entity;

/**
 * @author Supriyo M
 *
 */
public enum UserRole {
	
	ADMIN, USER

}

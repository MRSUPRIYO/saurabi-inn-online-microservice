/**
 * 
 */
package com.saurabi.online.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.saurabi.online.model.dto.CreateOrderDTO;
import com.saurabi.online.model.dto.ItemDTO;
import com.saurabi.online.model.dto.OrderDTO;
import com.saurabi.online.model.dto.UserDetailsDTO;

/**
 * @author Supriyo M
 *
 */

@FeignClient("user-service")
public interface RestaurantUserClient {
	
	@GetMapping("/")
	String home();
	
	@PostMapping("user/registerUser")
	ResponseEntity<String> registerUser(@Valid @RequestBody UserDetailsDTO userDetails);
	
	@GetMapping("userControl/viewMenu")
	List<ItemDTO> viewMenu();
	
	@PostMapping("userControl/placeOrder")
	ResponseEntity<String> placeOrder(@Valid @RequestBody CreateOrderDTO orderDetails,@RequestParam("email") String email);
	
	@GetMapping("userControl/viewFinalBill")
	OrderDTO viewFinalBill(@RequestParam("email") String email);	

}

package com.saurabi.online.service;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author Supriyo M
 *
 */
@FeignClient(name="festivesale-service")
@Lazy
public interface RestaurantFestiveSaleClient {
	
	@RequestMapping(value = "/surabi/festivesale/confirmorder", method = POST)
	String confirmorder(@RequestParam String userName,@RequestParam String festiveCode,@RequestParam(name = "orderCityName") String orderCityName) ;
}

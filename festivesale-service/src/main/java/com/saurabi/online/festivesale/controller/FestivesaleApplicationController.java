package com.saurabi.online.festivesale.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.saurabi.online.festivesale.service.FestivesaleApplicationService;

@RestController
@RequestMapping("/surabi/festivesale")
public class FestivesaleApplicationController {

	@Autowired
	FestivesaleApplicationService festivesaleApplicationService;

	@PostMapping("/confirmorder")
	public String confirmOrder(@RequestParam String userName, @RequestParam String festiveCode, String orderCityName) {
		return festivesaleApplicationService.confirmOrder(userName, 50, orderCityName);
	}

}

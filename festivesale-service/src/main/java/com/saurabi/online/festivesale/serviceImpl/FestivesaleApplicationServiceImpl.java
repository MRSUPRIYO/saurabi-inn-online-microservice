package com.saurabi.online.festivesale.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saurabi.online.festivesale.entity.RestaurantOrderDetails;
import com.saurabi.online.festivesale.repository.RestaurantOrderDetailsRepository;
import com.saurabi.online.festivesale.service.FestivesaleApplicationService;

/**
 * @author Supriyo M
 *
 */
@Service
public class FestivesaleApplicationServiceImpl implements FestivesaleApplicationService {

	@Autowired
	RestaurantOrderDetailsRepository restaurantOrderDetailsRepository;

	@Override
	public String confirmOrder(String userName, int discPerc, String cityName) {
		RestaurantOrderDetails restaurantOrderDetails;
		if (restaurantOrderDetailsRepository.existsByOrderStatusAndOrderUserName("CREATED", userName)) {
			restaurantOrderDetails = restaurantOrderDetailsRepository.findByOrderStatusAndOrderUserName("CREATED",
					userName);
			restaurantOrderDetails.setOrderCityName(cityName);
			restaurantOrderDetails.setOrderStatus("PAID");
			restaurantOrderDetails.setOrderPaymentMode("CARD");
			restaurantOrderDetails.setOrderDiscPerc(discPerc);
			restaurantOrderDetails.setOrderDiscAmount(discPerc * 0.01 * restaurantOrderDetails.getOrderBillAmount());
			restaurantOrderDetails.setOrderNetAmount(
					restaurantOrderDetails.getOrderBillAmount() - restaurantOrderDetails.getOrderDiscAmount());
			restaurantOrderDetailsRepository.save(restaurantOrderDetails);
			return "Order No : " + restaurantOrderDetails.getOrderId() + " for user " + userName
					+ " Confirmed and Bill Paid";
		} else {
			throw new RuntimeException("No order created for the user : " + userName + " to confirm and generate bill");
		}
	}

}

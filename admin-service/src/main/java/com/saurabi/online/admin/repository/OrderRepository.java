package com.saurabi.online.admin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.saurabi.online.admin.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

	@Query(value = "select max(id) from Orders where user_id=:userId")
	long getMaxOrderIdByUserId(@Param("userId") long userId);
		
	@Query(value = "select o from Orders o where CAST(created_at AS date)=CAST(CURRENT_TIMESTAMP AS date)")
	List<Order> findAllByCreatedAt();
	
	//@Query(value = "select sum(grand_total) from Orders where FUNC('MONTH',created_at)=FUNC('MONTH',CURRENT_TIMESTAMP)")
	@Query(value = "select o from Orders o where SUBSTRING(CAST(created_at AS date),1,7)= SUBSTRING(CAST(CURRENT_TIMESTAMP AS date),1,7)")
	List<Order> getTotalSalesOfCurrentMonth();
}

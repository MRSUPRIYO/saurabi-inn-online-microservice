package com.saurabi.online.admin.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.saurabi.online.admin.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

	Optional<User> findByEmail(String email);
}

package com.saurabi.online.admin.service;

import com.saurabi.online.admin.entity.User;
import com.saurabi.online.admin.exception.UserException;
import com.saurabi.online.admin.model.dto.UserDTO;

public interface UserService {

	User loadUserById(long id) throws UserException;

	boolean userExists(String email);

	User loadUserByEmail(String email) throws UserException;

	User createSingleUser(UserDTO userDetails);

	User updateSingleUser(UserDTO userDetails);

	void deleteSingleUser(String email) throws UserException;

}
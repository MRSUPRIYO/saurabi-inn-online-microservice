package com.saurabi.online.admin.service;

import java.util.List;

import com.saurabi.online.admin.exception.UserException;
import com.saurabi.online.admin.model.dto.OrderDTO;

public interface RestaurantService {

	List<OrderDTO> getAllBillsGeneratedToday() throws UserException;

	double getTotalSalesOfCurrentMonth();

}
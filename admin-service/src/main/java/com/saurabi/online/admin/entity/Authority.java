package com.saurabi.online.admin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "Authorities")
public class Authority implements GrantedAuthority {
	/**
	* 
	*/
	private static final long serialVersionUID = -8042490030975054816L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	
	private String authority;

	@ManyToOne
	@JoinColumn(name = "EMAIL", referencedColumnName = "EMAIL")
	private User userDetails;
	

	@Override
	public String getAuthority() {
		return this.authority;
	}
}
/**
 * 
 */
package com.saurabi.online.admin.entity;

/**
 * @author Supriyo M
 *
 */
public enum UserRole {
	
	ADMIN, USER

}

package com.saurabi.online.admin.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saurabi.online.admin.entity.Order;
import com.saurabi.online.admin.exception.UserException;
import com.saurabi.online.admin.model.dto.OrderDTO;
import com.saurabi.online.admin.repository.OrderRepository;
import com.saurabi.online.admin.service.RestaurantService;
import com.saurabi.online.admin.service.UserService;
import com.saurabi.online.admin.utility.AppUtility;

@Service
public class RestaurantServiceImp implements RestaurantService {

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	UserService userService;

	@Override
	public List<OrderDTO> getAllBillsGeneratedToday() throws UserException {
		List<Order> allOrdersGeneratedToday = orderRepository.findAllByCreatedAt();
		
		if (allOrdersGeneratedToday != null) {

			return allOrdersGeneratedToday.stream().map(AppUtility::getOrderDTOFromOrder).collect(Collectors.toList());
		}
		
		return null;
	}

	@Override
	public double getTotalSalesOfCurrentMonth() {
		
		double totalSalesOfThisMonth=0.0;
		
		List<Order> allOrdersGeneratedThisMonth = orderRepository.getTotalSalesOfCurrentMonth();
		
		for(Order orderOfThisMonth:  allOrdersGeneratedThisMonth ) {
			
			totalSalesOfThisMonth = totalSalesOfThisMonth+orderOfThisMonth.getGrandTotal();
		}
		
		return totalSalesOfThisMonth;
	}

}

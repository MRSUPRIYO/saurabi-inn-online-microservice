package com.saurabi.online.offlineuser.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Supriyo M
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantOrderDetailsFeedbackDTO {
	private long ordfOrderId;
	private String ordfFeedbackDesc;
}

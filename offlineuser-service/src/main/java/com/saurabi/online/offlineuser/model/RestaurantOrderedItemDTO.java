package com.saurabi.online.offlineuser.model;

import lombok.Getter;

/**
 * @author Supriyo M
 *
 */
@Getter
public class RestaurantOrderedItemDTO {
	private long itemId;
	private String itemName;
	private double price;
	private int quantity;
	private double amount;

	public RestaurantOrderedItemDTO(long itemId, String itemName, double price, int quantity) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.price = price;
		this.quantity = quantity;
		this.amount = price * quantity;
	}

}

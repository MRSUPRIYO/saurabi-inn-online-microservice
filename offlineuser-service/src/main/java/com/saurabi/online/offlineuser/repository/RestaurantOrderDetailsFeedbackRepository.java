package com.saurabi.online.offlineuser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saurabi.online.offlineuser.entity.RestaurantOrderDetailsFeedback;

/**
 * @author Supriyo M
 *
 */
@Repository
public interface RestaurantOrderDetailsFeedbackRepository extends JpaRepository<RestaurantOrderDetailsFeedback, Long> {

}

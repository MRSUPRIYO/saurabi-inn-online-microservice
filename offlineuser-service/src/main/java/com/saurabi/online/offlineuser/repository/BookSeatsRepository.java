package com.saurabi.online.offlineuser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saurabi.online.offlineuser.entity.BookSeats;

/**
 * @author Supriyo M
 *
 */
@Repository
public interface BookSeatsRepository extends JpaRepository<BookSeats, Long> {

}

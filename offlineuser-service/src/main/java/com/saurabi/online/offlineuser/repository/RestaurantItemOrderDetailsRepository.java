package com.saurabi.online.offlineuser.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saurabi.online.offlineuser.entity.RestaurantItemOrderDetails;

/**
 * @author Supriyo M
 *
 */
@Repository
public interface RestaurantItemOrderDetailsRepository extends CrudRepository<RestaurantItemOrderDetails, Long> {

}

package com.saurabi.online.offlineuser.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Supriyo M
 *
 */
@Entity
@Immutable
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantOrderDetailsFeedback {
	@Id
	@Column(name = "ordf_feedback_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ordfFeedbackId;

	@Column(name = "ordf_order_id")
	private long ordfOrderId;

	@Column(name = "ordf_feedback_desc")
	private String ordfFeedbackDesc;
}

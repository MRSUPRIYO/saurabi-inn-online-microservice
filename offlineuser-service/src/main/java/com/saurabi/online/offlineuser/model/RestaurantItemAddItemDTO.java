package com.saurabi.online.offlineuser.model;

import lombok.Data;
import lombok.ToString;

/**
 * @author Supriyo M
 *
 */

@Data
@ToString
public class RestaurantItemAddItemDTO {
	private long itemId;
	private int quantity;

	public RestaurantItemAddItemDTO(long itemId, int quantity) {
		this.itemId = itemId;
		this.quantity = quantity;
	}

	public RestaurantItemAddItemDTO() {

	}
}

package com.saurabi.online.offlineuser.service;

import java.time.LocalDate;

/**
 * @author Supriyo M
 *
 */
public interface OfflineuserApplicationService {
	public String bookSeats(String userName, int numberOfSeats, LocalDate bookDate);

	public String feedBack(long orderId, String Feedback);

	public String confirmOrder(String userName, String paymentMode, String orderCityName);
}

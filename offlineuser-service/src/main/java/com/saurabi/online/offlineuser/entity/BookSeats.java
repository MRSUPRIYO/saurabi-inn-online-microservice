package com.saurabi.online.offlineuser.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Supriyo M
 *
 */
@Entity
@Immutable
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookSeats {

	@Id
	@Column(name = "book_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long bookId;

	@Column(name = "username")
	private String userName;

	@Column(name = "number_of_seats")
	private int numberOfSeats;

	@Column(name = "book_date")
	private LocalDate bookDate;

}

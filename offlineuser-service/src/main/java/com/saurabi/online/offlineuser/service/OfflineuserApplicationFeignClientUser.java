package com.saurabi.online.offlineuser.service;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.time.LocalDate;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.saurabi.online.offlineuser.model.RestaurantGeneratedBillsDTO;
import com.saurabi.online.offlineuser.model.RestaurantOrderDetailsDTO;

/**
 * @author Supriyo M
 *
 */
@FeignClient(name = "user")
@Lazy
public interface OfflineuserApplicationFeignClientUser {
	@RequestMapping(value = "/surabi/user/addItemstoOrder", method = POST)
	public String addItemstoOrder(@RequestParam(name = "userName") String userName,
			@RequestBody RestaurantOrderDetailsDTO restaurantOrderDetailsDTO);

	@RequestMapping(value = "/surabi/user/menu", method = GET)
	public String getAllItems(@RequestParam(name = "userName") String userName);

	@RequestMapping(value = "/surabi/user/confirmorder", method = POST)
	public String confirmOrder(@RequestParam(name = "userName") String userName,
			@RequestParam(name = "orderCityName") String orderCityName);

	@RequestMapping(value = "/surabi/user/vieworder", method = GET)
	public List<RestaurantGeneratedBillsDTO> viewOrder(@RequestParam(name = "userName") String userName);

	@RequestMapping(value = "/surabi/user/viewgeneratedbills", method = GET)
	public List<RestaurantGeneratedBillsDTO> getGeneratedBills(
			@RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
			@RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate);

	@RequestMapping(value = "/surabi/user/registeruser", method = POST)
	public String registerUser(@RequestParam("userName") String userName, @RequestParam("password") String password);
}
